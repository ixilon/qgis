IMAGE=ixilon/qgis
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth

build:
	docker build -t ${IMAGE} .

run:
	xauth nlist "${DISPLAY}" | sed -e 's/^..../ffff/' | xauth -f ${XAUTH} nmerge -
	docker run --rm --interactive --tty --name qgis \
	  --env DISPLAY=unix${DISPLAY} \
	  --env XAUTHORITY=${XAUTH} \
	  --volume ${XAUTH}:${XAUTH} \
	  --volume ${XSOCK}:${XSOCK} \
	  ${IMAGE}
