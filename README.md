# QGIS - A Free and Open Source Geographic Information System #

[![Build Status](https://semaphoreci.com/api/v1/ixilon/qgis/branches/master/badge.svg)](https://semaphoreci.com/ixilon/qgis)

The QGIS desktop application as Docker container.

### Howto use this image ###

```
IMAGE=ixilon/qgis
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth

xauth nlist ${DISPLAY} | sed -e 's/^..../ffff/' | xauth -f ${XAUTH} nmerge -
docker run --rm --interactive --tty --name qgis \
  --env DISPLAY=unix${DISPLAY} \
  --env XAUTHORITY=${XAUTH} \
  --volume ${XAUTH}:${XAUTH} \
  --volume ${XSOCK}:${XSOCK} \
  ${IMAGE}
```

### Links ###

* [QGIS Homepage](http://www.qgis.org)
